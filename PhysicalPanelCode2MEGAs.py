# PhysicalPanelCode2MEGAs.py
# Make Arduino read digital input pins  hi/lo (will be physically a switch position on panel) and send to JMRI over serial
# so that JMRI changes the status of a sensor.
# JMRI will have to monitor sensor object and change status of corresponding turnout object.
# On turnout object status change, JMRI will send command to Arduino over serial to change LED indicator
# for turnout LED indicator and JMRI will have to send the corresponding DCC command.
# See readme at: https://gitlab.com/pjbca/dccplusplusphysicalpanel

# Peter Burke 6/11/2020
# Adapted from Geoff Bunza 2018
# url https://model-railroad-hobbyist.com/node/34417
# url https://model-railroad-hobbyist.com/node/34392


# PJB 8/3/2020
# MEGA1 is for controlling turnouts.
# Sends 0.1 s digital LOW pulse to a relay such as Sainsmart relay.
# For Sainsmart brand relay matrix, a low closes relay, which would send 12V pulse to turnout solonoid.
# Assumes ATXXX where xxx is the pin # on MEGA1 to activate low (2-69).
# MEGA2 is for LEDs on panel and switch input.
# For input, assumes ARxxx where xxx is the pin # and sensor #.
# MEGA1 uses PhysicalPanelMEGA1.ino sketch.
# MEGA2 uses PhysicalPanelMEGA2.ino sketch.


import jarray
import jmri
import java
import purejavacomm
import java.beans
import time

#*********************CONSTANTS*****************************************

Data_Pin_Max   = 70  # Max sensor pin NUMBER (plus one) Mega=70,UNO,Pro Mini,Nano=20
Data_Pin_Start  = 2  # Starting Sensor Pin number (usually 2 as 0/1 are TX/RX
# Creates a listener for turnout ATx with x from Data_Pin_Max to (Data_Pin_Start-1)


Sensor_Pin_Max =  70  # Max sensor pin NUMBER (plus one) Mega=70,UNO,Pro Mini,Nano=20
Sensor_Pin_Start = 2  # Starting Sensor Pin number (usually 2 as 0/1 are TX/RX


#*********************Wait for some time until getting down to business************

#time.sleep(30)

#******************CREATE AND OPEN COM PORT*****************************
print "creating and opening com port"

global extportin

#portname="ttyUSB0"
portname="tty100"
#portname="ttyACM1"
#portname="/dev/panelmega"

portID = purejavacomm.CommPortIdentifier.getPortIdentifier(portname)
try:
    port = portID.open("JMRI", 50)
except purejavacomm.PortInUseException:
    port = extportin

extportin = port
# set options on port
baudrate = 19200
port.setSerialPortParams(baudrate, 
purejavacomm.SerialPort.DATABITS_8, 
purejavacomm.SerialPort.STOPBITS_1, 
purejavacomm.SerialPort.PARITY_NONE)

inputStream = port.getInputStream()
outputStream = port.getOutputStream()
# Now inputStream and outputStream are in the global namespace and can presumably be used by any instance of any # object and hard coded into that objects code. 
#Although this is not good practice, we will do it here for convience rather than having to pass the port to # each object, which would be better practice but more coding work.

print "created and opened com port"
time.sleep(2)

#******************CREATE AND OPEN SECOND COM PORT*****************************

print "creating and opening second com port"

global extportin2

#portname="ttyUSB0"
portname2="tty102"
#portname="ttyACM1"
#portname="/dev/panelmega"

portID2 = purejavacomm.CommPortIdentifier.getPortIdentifier(portname2)
try:
    port2 = portID2.open("JMRI", 50)
except purejavacomm.PortInUseException:
    port2 = extportin2

extportin2 = port2
# set options on port
baudrate2 = 19200
port2.setSerialPortParams(baudrate2, 
purejavacomm.SerialPort.DATABITS_8, 
purejavacomm.SerialPort.STOPBITS_1, 
purejavacomm.SerialPort.PARITY_NONE)

inputStream2 = port2.getInputStream()
outputStream2 = port2.getOutputStream()
# Now inputStream and outputStream are in the global namespace and can presumably be used by any instance of any # object and hard coded into that objects code. 
#Although this is not good practice, we will do it here for convience rather than having to pass the port to # each object, which would be better practice but more coding work.

print "created and opened second com port"
time.sleep(2)

#******************DEFINE LISTENER CLASS THAT RESPONDS TO TURNOUT CHANGE*****************************
# A listener object.
# On turnout change (turnout ID# is a property of the object instance, i.e. specific to that object instance):
# On com1: (Causes MEGA1 to throw switch)
# Sends ATxx,0 then ATxx,1 to temporarily cause pin xx to go low and throw relay for pin # thrownrelayoutputpinnumber or closedrelayoutputpinnumber
# This relay pulse open causes 12V to solonoid for 0.1s which throws physical switch.
# On com2: (Causes MEGA2 to change LEDs on panel appropriately)
# Sends ATxx,0 or ATxx,1 to com2 to cause it to interpret and change LED

class TurnoutChangeListener(java.beans.PropertyChangeListener):
    
    def __init__(self, id, turnoutnumber,thrownrelayoutputpinnumber,closedrelayoutputpinnumber) : # seems value not used...
        self.id = id # not really used here
        self.turnoutnumber = turnoutnumber
        self.thrownrelayoutputpinnumber = thrownrelayoutputpinnumber # Arduino output pin for thrown relay
        self.closedrelayoutputpinnumber = closedrelayoutputpinnumber # Arduino output pin for closed relay
        self.name = "AT"+str(turnoutnumber)
        print "self.name=",self.name
        turnout = turnouts.provideTurnout(self.name)
        turnout.addPropertyChangeListener(self) 
        self.turnoutcommand="AT"+str(turnoutnumber)  # sends ATxxy to com2 for LED change xx turnout # y 0 or 1
        self.thrownrelayoutputpinname="AT"+str(thrownrelayoutputpinnumber)
        self.closedrelayoutputpinname="AT"+str(closedrelayoutputpinnumber)
        #turnout.setCommandedState(CLOSED) # Initialize turnout, not sure if I want that now...
        return
  
  # on a property change event, first see if 
  # right type, and then write appropriate
  # value to port based on new state
    def propertyChange(self, event):
        #print "change",event.propertyName
        #print "from", event.oldValue, "to", event.newValue
        #print "source systemName", event.source.systemName
        #print "source userName", event.source.userName
        if (event.newValue == CLOSED and event.oldValue != CLOSED and event.propertyName == "KnownState") :
            print "set CLOSED for", event.source.userName
            #outputStream.write(event.source.userName)
            time.sleep(0.2)
            outputStream2.write(self.turnoutcommand)
            outputStream2.write(",0") # for example, AT18,0 would be sent to Ardunino to set pin 18 low.
            print "sending this command to serial2 :", self.turnoutcommand, ",0"
            time.sleep(0.2) # give Arduino chance to respond to 1st command
            # New send commend for current pulse to close relay for a short time:
            outputStream.write(self.closedrelayoutputpinname)
            outputStream.write(",1") # for example, AT18,0 would be sent to Ardunino to set pin 18 low.
            print "sending this command to serial :", self.closedrelayoutputpinname, ",1"
            time.sleep(0.1) # relay will be connected for this long
            outputStream.write(self.closedrelayoutputpinname)
            outputStream.write(",0") # for example, AT18,0 would be sent to Ardunino to set pin 18 low.
            print "sending this command to serial :", self.closedrelayoutputpinname, ",0"
            
            
        if (event.newValue == THROWN and event.oldValue != THROWN and event.propertyName == "KnownState") :
            print "set THROWN for", event.source.userName
            #outputStream.write(event.source.userName)
            time.sleep(0.2)
            outputStream2.write(self.turnoutcommand)
            outputStream2.write(",1") # for example, AT18,0 would be sent to Ardunino to set pin 18 low.
            print "sending this command to serial2 :", self.turnoutcommand, ",1"
            time.sleep(0.2) # give Arduino chance to respond to 1st command
            # New send commend for current pulse to close relay for a short time:
            outputStream.write(self.thrownrelayoutputpinname)
            outputStream.write(",1") # for example, AT18,0 would be sent to Ardunino to set pin 18 low.
            print "sending this command to serial :", self.thrownrelayoutputpinname, ",1"
            time.sleep(0.1) # relay will be connected for this long
            outputStream.write(self.thrownrelayoutputpinname)
            outputStream.write(",0") # for example, AT18,0 would be sent to Ardunino to set pin 18 low.
            print "sending this command to serial :", self.thrownrelayoutputpinname, ",0"
        return




#******************DEFINE LISTENER CLASS THAT RESPONDS TO SENSOR CHANGE DOWN TO THROW TURNOUT ONLY*****************************
# A listener object.
# On sensor change (sensornumber):
# Change corresponding turnout  object in JMRI(turnoutnumber) to THROWN.

class SensorToThrowTurnoutListener(java.beans.PropertyChangeListener):

  def __init__(self,id,turnoutnumber,sensornumber) :
    # From input parameters:
    #super(SensorToCloseTurnoutListener, self).__init__(id)
    self.id = id
    self.sensornumber = sensornumber
    self.turnoutnumber = turnoutnumber
    self.sensorname = "AR"+str(sensornumber)
    sensor = sensors.provideSensor(self.sensorname)
    sensor.addPropertyChangeListener(self)
    self.turnoutname = "AT"+str(self.turnoutnumber)
    turnout = turnouts.provideTurnout(self.turnoutname)
    return


  def propertyChange(self, event):
    print "-----------------------"
    print"SensorToThrowTurnoutListener called"      
    if (event.propertyName == "KnownState") :
      if (event.newValue == INACTIVE and event.oldValue != INACTIVE) :
        print "newValue == INACTIVE for", event.source.userName
        # Sensor became inactive. (Voltage went to low on pin on Adruino, indicating switch momentarily depressed). Do something.
        # Change turnout to closed or open depending on ...
        turnout = turnouts.provideTurnout(self.turnoutname)
        turnout.setCommandedState(THROWN)
    print"SensorToThrowTurnoutListener returning"      
    print "-----------------------"
    return

#******************DEFINE LISTENER CLASS THAT RESPONDS TO SENSOR CHANGE DOWN TO CLOSE TURNOUT ONLY*****************************
# A listener object.
# On sensor change (sensornumber):
# Change corresponding turnout  object in JMRI(turnoutnumber) to CLOSED.

class SensorToCloseTurnoutListener(java.beans.PropertyChangeListener):

  def __init__(self,id,turnoutnumber,sensornumber) :
    # From input parameters:
    #super(SensorToCloseTurnoutListener, self).__init__(id)
    self.id = id
    self.sensornumber = sensornumber
    self.turnoutnumber = turnoutnumber
    self.sensorname = "AR"+str(sensornumber)
    sensor = sensors.provideSensor(self.sensorname)
    sensor.addPropertyChangeListener(self)
    self.turnoutname = "AT"+str(self.turnoutnumber)
    turnout = turnouts.provideTurnout(self.turnoutname)
    return

  def propertyChange(self, event):
    print "-----------------------"
    print"SensorToCloseTurnoutListener called"
    if (event.propertyName == "KnownState") :
      if (event.newValue == INACTIVE and event.oldValue != INACTIVE) :
        print "newValue == INACTIVE for", event.source.userName
        # Sensor became inactive. (Voltage went to low on pin on Adruino, indicating switch momentarily depressed). Do something.
        # Change turnout to closed or open depending on ...
        turnout = turnouts.provideTurnout(self.turnoutname)
        turnout.setCommandedState(CLOSED)
    print"SensorToCloseTurnoutListener returning"
    print "-----------------------"
    return


#****DEFINE SENSORS CLASS THAT READS SERIAL AND CHANGES CORRESPONDING SENSOR OBJECT****************
# An automaton object.
# Sends !!! at start/init
# Reads serial, if "A" in byte, parses
# Sets sensor object in JMRI based on read serial message
# UserName of sensor object will be ARy, where y is sensor #.
# Serial message format incoming assumed Ax where x is 7 bit # and 8th bit is 0/1 for on/off

class SerialSensorMux(jmri.jmrit.automat.AbstractAutomaton) :
    # init() is the place for your initialization
    def init(self) : # send !!! to Arduino to "wake it up?"
        print "Starting writing !!! to serial"
        time.sleep(0.2)
        outputStream2.write('!')
        time.sleep(0.1)
        outputStream2.write('!')
        time.sleep(0.1)
        outputStream2.write('!')
        time.sleep(0.1)
        outputStream2.write(0x0D) # return 10wcharacter
        time.sleep(0.2)
        # delays because the poor Arduino brain is so slow....
        return
    # handle() is called repeatedly until it returns false.
    def handle(self) :
        # get next character    
        if inputStream2.read() != 65 : # A = 65 in binary
                return 1
        sensor_num = inputStream2.read()
        sensor_state = ( sensor_num>>7 ) & 1 # state is everything after first 7 bits
        sensor_num = sensor_num & 0x7f # first 7 bits sensor num
        mesg = "AR%d" % (sensor_num)
        print "serial received ",mesg
        if sensor_num == 1 and sensor_state == 1 :
            print "Sensor Read Ends"
            #inputStream.close()
            #outputStream.close()
            #portID.close()
            return 0
        s = sensors.getByUserName( mesg )
        if s is None :
                print mesg, " Not Available"
                return 1
        if sensor_state == 1 :
                s.setKnownState(ACTIVE)
        if sensor_state == 0 :
                s.setKnownState(INACTIVE)

        return 1    # to continue 0 to Kill Script




#*************INSTANTIATE LISTENER CLASS OBJECTS THAT RESPOND TO SENSOR REV 2*****************************

print "################## creating sensor listeners #####################"


# Turnout XYZ
#SensorToThrowTurnoutListener(ID,TURNOUT,SENSORTOTHROW) # ID, Turnout #, Sensor pin # to throw
#SensorToCloseTurnoutListener(ID,TURNOUT,SENSORTOCLOSE)  # ID, Turnout #, Sensor pin # to close
#TurnoutChangeListener(ID,TURNOUT,OUTPUTPINTOTHROW,OUTPUTPINTOCLOSE) # ID, Turnout #, Output pin # to throw, output pin to close #


# Turnout 1
SensorToThrowTurnoutListener(1,1,2) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(2,1,3)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(3,1,2,3) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 2
SensorToThrowTurnoutListener(4,2,4) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(5,2,5)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(6,2,4,5) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 3
SensorToThrowTurnoutListener(7,3,6) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(8,3,7)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(9,3,6,7) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 4
SensorToThrowTurnoutListener(10,4,8) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(11,4,9)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(12,4,8,9) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 5
SensorToThrowTurnoutListener(13,5,10) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(14,5,11)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(15,5,10,11) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 10
SensorToThrowTurnoutListener(16,10,12) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(17,10,46)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(18,10,12,46) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 11
SensorToThrowTurnoutListener(19,11,14) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(20,11,15)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(21,11,14,15) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 13
SensorToThrowTurnoutListener(22,13,16) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(23,13,17)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(24,13,16,17) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 14
SensorToThrowTurnoutListener(25,14,18) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(26,14,19)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(27,14,18,19) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 12
SensorToThrowTurnoutListener(28,12,24) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(29,12,25)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(30,12,24,25) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 20
SensorToThrowTurnoutListener(31,20,22) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(32,20,23)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(33,20,22,23) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 40
SensorToThrowTurnoutListener(34,40,26) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(35,40,27)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(36,40,26,27) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 41
SensorToThrowTurnoutListener(37,41,28) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(38,41,29)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(39,41,28,29) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 42
SensorToThrowTurnoutListener(40,42,30) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(41,42,31)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(42,42,30,31) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 30
SensorToThrowTurnoutListener(43,30,32) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(44,30,33)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(45,30,32,33) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 31
SensorToThrowTurnoutListener(46,31,34) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(47,31,35)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(48,31,34,35) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 32
SensorToThrowTurnoutListener(49,32,36) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(50,32,37)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(51,32,36,37) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 50
SensorToThrowTurnoutListener(52,50,38) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(53,50,39)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(54,50,38,39) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 51
SensorToThrowTurnoutListener(55,51,40) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(56,51,41)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(57,51,40,41) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 52
SensorToThrowTurnoutListener(58,52,42) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(59,52,43)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(60,52,42,43) # ID, Turnout #, Output pin # to throw, output pin to close #

# Turnout 53
SensorToThrowTurnoutListener(61,53,44) # ID, Turnout #, Sensor pin # to throw
SensorToCloseTurnoutListener(62,53,45)  # ID, Turnout #, Sensor pin # to close
TurnoutChangeListener(63,53,44,45) # ID, Turnout #, Output pin # to throw, output pin to close #






print "###################### created sensor listeners ##########################"

#*************INSTANTIATE SENSOR CLASS OBJECT*********************************
#**************THAT READS SERIAL AND CHANGES CORRESPONDING SENSOR OBJECT *****************************

print "################### creating SerialSensorMux automaton ############################"
a = SerialSensorMux()
print "################### created SerialSensorMux automaton ############################"


# set the thread name, so easy to cancel if needed
a.setName("Physical panel script")

# start running
print "################### creating starting SerialSensorMux automaton ############################"
a.start();
print "################### created started SerialSensorMux automaton ############################"



print "finished creating and starting SerialSensorMux automaton"

#******************Initiate all turnouts to their assigned status now**************************

# Mega2 code in loop hangs until we give it at least one command. Give it a command to get strated, then wait for things to settle.
print"Mega2 code in loop hangs until we give it at least one command. Give it a command to get strated, then wait for things to settle."

turnoutname="AT"+str(1)
turnout = turnouts.provideTurnout(turnoutname)
turnout.setCommandedState(CLOSED) # Initialize turnout, not sure if I want that now...
time.sleep(1)
turnout.setCommandedState(THROWN) # Initialize turnout, not sure if I want that now...

print "###################### waiting for things to settle 2 secs ##########"
time.sleep(2)
print "###################### waited for things to settle 2 secs ##########"


print "###################### INITIALIZING ALL TURNOUTS ##########"
for i in range (1,69) :
    
        print "######## closing turnout number ################ ", i
        turnoutname="AT"+str(i)
        turnout = turnouts.provideTurnout(turnoutname)
        turnout.setCommandedState(CLOSED) # Initialize turnout, not sure if I want that now...
        print "######## closed turnout number ################ ", i
        #time.sleep(0.2)
        i = i + 1


print "###################### FINE TUNING INITILIZATION OF TURNOUTS ##########"
# for coninuout loop, nee 5,12,40 actually in the thrown position

turnoutname="AT"+str(5)
turnout = turnouts.provideTurnout(turnoutname)
turnout.setCommandedState(THROWN) # Initialize turnout, not sure if I want that now...
time.sleep(0.2)

turnoutname="AT"+str(12)
turnout = turnouts.provideTurnout(turnoutname)
turnout.setCommandedState(THROWN) # Initialize turnout, not sure if I want that now...
time.sleep(0.2)

turnoutname="AT"+str(40)
turnout = turnouts.provideTurnout(turnoutname)
turnout.setCommandedState(THROWN) # Initialize turnout, not sure if I want that now...
time.sleep(0.2)


print "###################### INITIALED ALL TURNOUTS ##########"
