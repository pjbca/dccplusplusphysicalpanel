// PhysicalPanel
// Designed to go along with jython JMRI script xxx and an Arduino.
// For monitoring switches on a panel, lighting LED of the panel , and throwing turnouts.
// Peter Burke 6/11/2020
// Adapted from Geoff Bunza 2018
// url https://model-railroad-hobbyist.com/node/34417b
// url https://model-railroad-hobbyist.com/node/34392



//*******************CONSTANTS**********************************************

#define Sensor_Pin_Max   70  // Max sensor pin NUMBER (plus one) Mega=70,UNO,Pro Mini,Nano=20
#define Sensor_Pin_Start  2  // Starting Sensor Pin number (usually 2 as 0/1 are TX/RX
#define Sensor_Offset     0  // This Offset will be ADDED to the value of each Sensor_Pin to determine the sensor
// number sent to JMRI, so pin D12 will set sensor AR:(12+Sensor_Offset) in JMRI
// This would allow one Arduino Sensor channel to set sensors 2-69 and another to
// Set sensors 70-137 for example; this offset can also be negative
#define Sensors_Active_Low 0 // Set Sensors_Active_Low to 1 if sensors are active LOW
// Set Sensors_Active_Low to 0 if sensors are active HIGH

#define open_delay 15        // longer delay to get past script initialization
#define delta_delay 4        // Short delay to allow the script to get all the characters
#define delta_delay_2 1000        // Long delay to make sure switches dont get energized too often and burn out

#define port_delay 600      // Serial Port delay (microsec) for error compensation - increase this if you get funny data at high speeds

bool  current_turnout_is_a_xover;
int current_turnout_indexnumber;
int current_LED_pin_number;
int current_board_number;
int turnoutnumbers[21] = {1, 2, 3, 4, 5, 10, 11, 13, 14, 12, 20, 40, 41, 42, 30, 31, 32, 50, 51, 52, 53};
// turnout index is for example: turnnout[5]=10 Here 5 is the index, 10 is the turnout #.

int boardnumber[21] = {8, 1, 5, 5, 5, 3, 3, 4, 4, 4, 4, 2, 2, 6, 2, 1, 1, 7, 7, 7, 7}; // board # as a function of turnout index


int  pins[21][8] =// pins as a fn of turnout index
{
  {0, 1, 2, 3, 4, 5, 6, 7},
  {8, 9, 10, 11, 12, 13, 14, 15},
  {0, 1, 2, 3, -1, -1, -1, -1},
  {4, 5, 6, 7, 8, 9, 10, 11},
  {12, 13, 14, 15, -1, -1, -1, -1},
  {0, 1, 2, 3, 4, 5, 6, 7},
  {8, 9, 10, 11, 12, 13, 14, 15},
  {0, 1, 2, 3, -1, -1, -1, -1},
  {12, 13, 14, 15, -1, -1, -1, -1},
  {8, 9, 10, 11, -1, -1, -1, -1},
  {4, 5, 6, 7, -1, -1, -1, -1},
  {0, 1, 2, 3, -1, -1, -1, -1},
  {4, 5, 6, 7, 8, 9, 10, 11},
  {0, 1, 2, 3, 4, 5, 6, 7},
  {12, 13, 14, 15, -1, -1, -1, -1},
  {0, 1, 2, 3, -1, -1, -1, -1},
  {4, 5, 6, 7, -1, -1, -1, -1},
  {0, 1, 2, 3, -1, -1, -1, -1},
  {4, 5, 6, 7, -1, -1, -1, -1},
  {8, 9, 10, 11, -1, -1, -1, -1},
  {12, 13, 14, 15, -1, -1, -1, -1}
};

float green_intensity = 0.03;
float red_intensity = 0.2;

//******* LED DRIVER STUFF************************8



#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver pwm1 = Adafruit_PWMServoDriver(0x40);
Adafruit_PWMServoDriver pwm2 = Adafruit_PWMServoDriver(0x41);
Adafruit_PWMServoDriver pwm3 = Adafruit_PWMServoDriver(0x42);
Adafruit_PWMServoDriver pwm4 = Adafruit_PWMServoDriver(0x43);
Adafruit_PWMServoDriver pwm6 = Adafruit_PWMServoDriver(0x44);
Adafruit_PWMServoDriver pwm7 = Adafruit_PWMServoDriver(0x45);
Adafruit_PWMServoDriver pwm5 = Adafruit_PWMServoDriver(0x46);
Adafruit_PWMServoDriver pwm8 = Adafruit_PWMServoDriver(0x47);
// see https://learn.adafruit.com/16-channel-pwm-servo-driver?view=all for addressing scheme


//**********************VARIABLES****************************************8

int i;

char  sensor_state [70];     // up to 70 sensors on a Mega2560
char  new_sensor_state ;     // temp to process the possible state change



int j;
char  firstByte ;           // temp to process the possible state change
char  secondByte ;
char  incomingByte = 0;     // working temp for character processing
int   turnout = 99;


//*******************SETUP**********************************************

void setup() {

 

  //**************************** LED driver setup*************************

  pwm1.begin();
  pwm1.setPWMFreq(100);  // This is the maximum PWM frequency
  pwm2.begin();
  pwm2.setPWMFreq(100);  // This is the maximum PWM frequency
  pwm3.begin();
  pwm3.setPWMFreq(100);  // This is the maximum PWM frequency
  pwm4.begin();
  pwm4.setPWMFreq(100);  // This is the maximum PWM frequency
  pwm5.begin();
  pwm5.setPWMFreq(100);  // This is the maximum PWM frequency
  pwm6.begin();
  pwm6.setPWMFreq(100);  // This is the maximum PWM frequency
  pwm7.begin();
  pwm7.setPWMFreq(100);  // This is the maximum PWM frequency
  pwm8.begin();
  pwm8.setPWMFreq(100);  // This is the maximum PWM frequency

  //**************************** LED driver flash to show init*************************


  //  turnOnALLLEDs();
  //  delay(100);
  turnOffALLLEDs();
  turnOnALLGreenLEDs();
  delay(500);
  turnOffALLLEDs();
  delay(500);
  turnOnALLRedLEDs();
  delay(500);
  turnOffALLLEDs();
  //delay(500);
  //  turnOnALLGreenLEDsWithIntensity(0.1);
  //  for (int i = 0; i < 10; i++) { // iterate over LEDs
  //    turnOnALLGreenLEDsWithIntensity(i / 10);
  //    delay(10);
  //  }
  //  turnOffALLLEDs();
  //  delay(100);
  //wave(10);
  delay(100);
  //dotogglecheck(1);


  //********************* Open serial connection

  Serial.begin(19200);              // Open serial connection.

  //*******************Wait for JMRI to send !!! init command over serial*****************************************

  while (Serial.available() == 0);  // wait until we get a charater from JMRI
  incomingByte = Serial.read();     // get the first character
  while ((Serial.available() > 0) && (incomingByte != '!')) incomingByte = Serial.read(); //get past !!!
  while ((Serial.available() > 0) ) incomingByte = Serial.read();                     //flush anything else

  delay(open_delay);                 // take a breath



  //*******************Sensors setup*****************************************
  //  Initialize pins Sensor_Pin_Start to Sensor_Pin_Max as INPUT_PULLUP
  //  Read pin state into sensor_state[i] array.
  //  Report values to JMRI over serial interface.

  for ( i = Sensor_Pin_Start; i < Sensor_Pin_Max; i++)  { //Initialize all sensors in JMRI and grab each sensor
    pinMode(i, INPUT_PULLUP);       // define each sensor pin as coming in
    sensor_state[i] = (digitalRead( i ))^Sensors_Active_Low;    // read & save each sensor state & invert if necessary
    Serial.print("A"); Serial.print (char((sensor_state[i] << 7) + i)); // send "A <on/off><snesor #>" to JMRI script
    delay(delta_delay);             // in milliseconds, take a short breath as not to overwhelm JMRI's seraial read
  }


}

//*******************LOOP**********************************************

void loop() {
  //*******************Sensors code*****************************************

  //  Loop over Sensor_Pin_Start to Sensor_Pin_Max:
  //    Read sensor pin, if changed, update sensor_state[i] array and report value to JMRI over serial interface.

  for ( i = Sensor_Pin_Start; i < Sensor_Pin_Max; i++)  { // scan every sensor over and over for any sensor changes

    new_sensor_state = (digitalRead( i ))^Sensors_Active_Low;  // read & save each sensor state & invert if necessary

    if (new_sensor_state != sensor_state[i] )  {               // check if the sensor changed ->if yes update JMRI
      Serial.print("A"); Serial.print (char((new_sensor_state << 7) + i)); // send "A <on/off><snesor #>" to JMRI script
      sensor_state[i] = new_sensor_state ;                     // save the updated sensor state
      delay(delta_delay_2);            // in milliseconds, take a short breath as not to overwhelm JMRI's seraial read

    }
  }
  //*******************Turnouts read code*****************************************

  if (Serial.available() > 0)  {  // wait until we get a charater from JMRI
    // digitalWrite(LED_BUILTIN, LOW);   // turn the LED on (HIGH is the voltage level)

    // get the first character
    firstByte = Serial.read();
    delayMicroseconds(port_delay);
    while ((firstByte != 'A') && (Serial.available())) { // bad code, gets hung here if A never appears
      firstByte = Serial.read();
      delayMicroseconds(port_delay);
    }
    if (Serial.available()) {
      secondByte = Serial.read(); // get the second character
    }
    if ((firstByte == 'A') && (secondByte == 'T'))  {
      j = 0;
      turnout = Serial.parseInt();
      if (Serial.available()) {
        incomingByte = Serial.read();
      }
      delayMicroseconds(port_delay);
      if (Serial.available()) {
        incomingByte = Serial.read();
      }

//*******************Turnouts LED set code*****************************************

// current_turnout_is_a_xover

      
      
      
      //  turnout is the turnout #.
      // indexfromturnout(turnout) is the index. Need a function for that.
      //if(turnout==3){wave(1);}
      current_turnout_indexnumber = indexfromturnout(turnout);
      //if(current_turnout_indexnumber==0){wave(10);}
      //current_turnout_indexnumber=0;
      current_board_number = boardnumber[current_turnout_indexnumber];
      //if(current_board_number==7){wave(10);}
      //current_board_number=1;
      if (pins[current_turnout_indexnumber][7]!= -1){
        current_turnout_is_a_xover=true;
        if (incomingByte == '1') {// change to thrown
          //wave(1);
          current_LED_pin_number = pins[current_turnout_indexnumber][0];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,red_intensity);
          current_LED_pin_number = pins[current_turnout_indexnumber][1];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][2];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,red_intensity);
          current_LED_pin_number = pins[current_turnout_indexnumber][3];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][4];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][5];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,green_intensity);
          current_LED_pin_number = pins[current_turnout_indexnumber][6];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][7];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,green_intensity);
        }
        if (incomingByte == '0') {// change to closed
          current_LED_pin_number = pins[current_turnout_indexnumber][0];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][1];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,green_intensity);
          current_LED_pin_number = pins[current_turnout_indexnumber][2];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][3];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,green_intensity);
          current_LED_pin_number = pins[current_turnout_indexnumber][4];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][5];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,green_intensity);
          current_LED_pin_number = pins[current_turnout_indexnumber][6];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][7];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,green_intensity);
        }
      }
      if (pins[current_turnout_indexnumber][7]== -1){
        current_turnout_is_a_xover=false;
        if (incomingByte == '1') {// change to thrown
          current_LED_pin_number = pins[current_turnout_indexnumber][0];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,red_intensity);
          current_LED_pin_number = pins[current_turnout_indexnumber][1];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][2];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][3];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,green_intensity);
        }
        if (incomingByte == '0') {// change to closed
          current_LED_pin_number = pins[current_turnout_indexnumber][0];
          turnOffLED(current_LED_pin_number, current_board_number);
          current_LED_pin_number = pins[current_turnout_indexnumber][1];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,green_intensity);
          current_LED_pin_number = pins[current_turnout_indexnumber][2];
          turnOnLEDwithIntensity(current_LED_pin_number, current_board_number,red_intensity);
          current_LED_pin_number = pins[current_turnout_indexnumber][3];
          turnOffLED(current_LED_pin_number, current_board_number);
        }
        
      }

        
      
     // for (int i = 0; i < 8; i++) { // iterate over pins
     //   current_LED_pin_number = pins[current_turnout_indexnumber][i];
     //   if (current_LED_pin_number != -1) {
     //     if (incomingByte == '1'){ // change to thrown
     //       if ((current_LED_pin_number % 2) == 0){ // even #
     //       turnOnLED(current_LED_pin_number, current_board_number);
     //       }
     //     }
     //     if (incomingByte == '0'){ // change to closed
     //       if ((current_LED_pin_number % 2) != 0){ // odd #
     //       turnOnLED(current_LED_pin_number, current_board_number);
     //       }
     //     }
     //     if (incomingByte == '0'){ // change to closed
     //       if ((current_LED_pin_number % 2) == 0){ // even #
     //       turnOnLED(current_LED_pin_number, current_board_number);
     //       }
     //     }
     //     if (incomingByte == '1'){ // change to thrown
     //       if ((current_LED_pin_number % 2) != 0){ // odd #
     //       turnOnLED(current_LED_pin_number, current_board_number);
     //       }
     //     }          
     // }
   // }

    // int boardnumber[21]; // board # as a function of turnout index
    // int  pins[21][8] =// pins as a fn of turnout index
    // int turnOnLED(int LEDnumber, int boardnumber)
    // int turnOffLED(int LEDnumber, int boardnumber)

    // Have 2-69 as turnout #s in JMRI.
    // We have 16x4=64 PWM outputs, but need 2 per turnout regular, 4 per xover.
    // Need numbering scheme.
    // See google sheet. Could just do a big if then function.
    // Call toggleLEDsFromTurnoutNumberAndState(int turnoutnumber, int state)





  }
}

}

int turnOffLED(int LEDnumber, int boardnumber) {


  if (boardnumber == 1) {
    pwm1.setPWM(LEDnumber, 0, 4096); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 2) {
    pwm2.setPWM(LEDnumber, 0, 4096); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 3) {
    pwm3.setPWM(LEDnumber, 0, 4096); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 4) {
    pwm4.setPWM(LEDnumber, 0, 4096); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 5) {
    pwm5.setPWM(LEDnumber, 0, 4096); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 6) {
    pwm6.setPWM(LEDnumber, 0, 4096); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 7) {
    pwm7.setPWM(LEDnumber, 0, 4096); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 8) {
    pwm8.setPWM(LEDnumber, 0, 4096); // Drive PWM LEDnumber hi;
  }



  return 1;
}


int turnOnLEDwithIntensity(int LEDnumber, int boardnumber, float intensity) {
  // intensity between 0 and 1
  int turnoffat;
  int turnonat = 1;
  turnoffat = intensity * 4095.0;
  // turnoffat = 410;


  if (boardnumber == 1) {
    pwm1.setPWM(LEDnumber, turnonat, turnoffat); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 2) {
    pwm2.setPWM(LEDnumber, turnonat, turnoffat); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 3) {
    pwm3.setPWM(LEDnumber, turnonat, turnoffat); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 4) {
    pwm4.setPWM(LEDnumber, turnonat, turnoffat); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 5) {
    pwm5.setPWM(LEDnumber, turnonat, turnoffat); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 6) {
    pwm6.setPWM(LEDnumber, turnonat, turnoffat); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 7) {
    pwm7.setPWM(LEDnumber, turnonat, turnoffat); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 8) {
    pwm8.setPWM(LEDnumber, 1, turnoffat); // Drive PWM LEDnumber hi;
  }



  return 1;
}

void turnOnLED(int LEDnumber, int boardnumber) {


  if (boardnumber == 1) {
    pwm1.setPWM(LEDnumber, 4096, 0); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 2) {
    pwm2.setPWM(LEDnumber, 4096, 0); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 3) {
    pwm3.setPWM(LEDnumber, 4096, 0); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 4) {
    pwm4.setPWM(LEDnumber, 4096, 0); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 5) {
    pwm5.setPWM(LEDnumber, 4096, 0); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 6) {
    pwm6.setPWM(LEDnumber, 4096, 0); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 7) {
    pwm7.setPWM(LEDnumber, 4096, 0); // Drive PWM LEDnumber hi;
  }
  if (boardnumber == 8) {
    pwm8.setPWM(LEDnumber, 4096, 0); // Drive PWM LEDnumber hi;
  }



  return;
}

int turnOnALLLEDs() {


  for (int j = 1; j < 9; j++) { // iterate over boards
    for (int i = 0; i < 16; i++) { // iterate over LEDs
      turnOnLED(i, j);
    }
  }

  return 1;
}



int turnOffALLLEDs() {


  for (int j = 1; j < 9; j++) { // iterate over boards
    for (int i = 0; i < 16; i++) { // iterate over LEDs
      turnOffLED(i, j);
    }
  }

  return 1;
}


int turnOnALLRedLEDs() { // odd pin number

  for (int j = 1; j < 9; j++) { // iterate over boards
    for (int i = 0; i < 16; i++) { // iterate over LEDs
      if ( (i % 2) == 0) {
        turnOnLEDwithIntensity(i, j,red_intensity);  // even red
      }
      if ( (i % 2) != 0) {
        turnOffLED(i, j);  // odd green
      }
    }
  }

  return 1;
}



int turnOnALLGreenLEDs() {

  for (int j = 1; j < 9; j++) { // iterate over boards
    for (int i = 0; i < 16; i++) { // iterate over LEDs
      if ( (i % 2) == 0) {
        turnOffLED(i, j);  // even red
      }
      if ( (i % 2) != 0) {
        turnOnLEDwithIntensity(i, j,green_intensity);  // odd green
      }
    }
  }

  return 1;
}

int turnOnALLGreenLEDsWithIntensity(float intensity) {

  for (int j = 1; j < 9; j++) { // iterate over boards
    for (int i = 0; i < 16; i++) { // iterate over LEDs
      if ( (i % 2) == 0) {
        turnOffLED(i, j);  // even red
      }
      if ( (i % 2) != 0) {
        turnOnLEDwithIntensity(i, j, intensity);  // odd green
      }
    }
  }

  return 1;
}


int wave(int delay_between_LEDs_in_ms) {

  for (int j = 1; j < 9; j++) { // iterate over boards
    for (int i = 0; i < 16; i++) { // iterate over LEDs
      if ( (i % 2) != 0) {
        turnOnLEDwithIntensity(i, j,green_intensity);  // odd green
      }
      delay(delay_between_LEDs_in_ms);
    }
  }
  for (int j = 1; j < 9; j++) { // iterate over boards
    for (int i = 0; i < 16; i++) { // iterate over LEDs
      turnOffLED(i, j);
      delay(delay_between_LEDs_in_ms);
    }
  }
  for (int j = 1; j < 9; j++) { // iterate over boards
    for (int i = 0; i < 16; i++) { // iterate over LEDs
      if ( (i % 2) == 0) {
        turnOnLEDwithIntensity(i, j,red_intensity);  // even red
      }
      delay(delay_between_LEDs_in_ms);
    }
  }
  for (int j = 1; j < 9; j++) { // iterate over boards
    for (int i = 0; i < 16; i++) { // iterate over LEDs
      turnOffLED(i, j);
      delay(delay_between_LEDs_in_ms);
    }
  }

  return 1;
}

int indexfromturnout(int turnout) {
  for (int i = 0; i < 21; i++) { // iterate over indices
    if (turnoutnumbers[i] == turnout) {
      return i;
    }
  }
  //return -1;
}


int dotogglecheck(int num_toggles) {
  // Toggle specific LEDs hard coded to check them.

  // Toggle board 8 pin 0
  turnOnLEDwithIntensity(0, 8,red_intensity);  // even red
  delay(1000);
  turnOffLED(0, 8);  // even red
  delay(1000);



  // Toggle board 8 pin 2
  turnOnLEDwithIntensity(2, 8,red_intensity);  // even red
  delay(1000);
  turnOffLED(2, 8);  // even red
  delay(1000);

  

  // Toggle board 8 pin 4
  turnOnLEDwithIntensity(4, 8,red_intensity);  // even red
  delay(1000);
  turnOffLED(4, 8);  // even red
  delay(1000);

  

  // Toggle board 8 pin 6
  turnOnLEDwithIntensity(6, 8,red_intensity);  // even red
  delay(1000);
  turnOffLED(6, 8);  // even red
  delay(1000);

  // Toggle board 1 pin 8
  turnOnLEDwithIntensity(8, 1,red_intensity);  // even red
  delay(1000);
  turnOffLED(8, 1);  // even red
  delay(1000);

 // Toggle board 1 pin 10
  turnOnLEDwithIntensity(10, 1,red_intensity);  // even red
  delay(1000);
  turnOffLED(10, 1);  // even red
  delay(1000);

  // Toggle board 1 pin 12
  turnOnLEDwithIntensity(12, 1,red_intensity);  // even red
  delay(1000);
  turnOffLED(12, 1);  // even red
  delay(1000);

  // Toggle board 1 pin 14
  turnOnLEDwithIntensity(14, 1,red_intensity);  // even red
  delay(1000);
  turnOffLED(14, 1);  // even red
  delay(1000);

  return 1;

}
