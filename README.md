# DCCplusplusPhysicalPanel

Arduino sketch and python/jython script to interact with physical switches and LEDs on a panel controlling a train layout, using JMRI.

# Overview:
My goal is to have a train set with turnouts controlled with a physical control panel and also JMRI over the computer. The throttle is DCC++ or Digitrax, but the turnouts are what are of interest to the control panel.

The control panel should have indicator LEDS red/green for where the train will go based on the switch. There will be one switch per turnout or crossover. The switch will be momentary and change the turnout (open or thrown), which will be indicated by the LEDs on the panel.

The switches are sensed by an Arduino MEGA connected to a Raspberry Pi over USB. (When a pin is brought LOW, the Arduino sends that information to JMRI on the Pi over serial.) The Raspberry Pi runs JMRI.

JMRI has a turnout object for each physical turnout, a sensor object for each physical switch.

When the Arduino senses a switch is toggled (by sensing if a pin is temporarily connected to ground), it sends over serial a message to JMRI. A JMRI script written in jython is running (“listener object” more on that below) that receives the serial message and changes the turnout to thrown or closed depending. Inside JMRI an automaton/listener object monitors all the turnout objects and if one changes state, it puts out a 0.1 s digital low pulse on one of the pins of the Arduino. (By sending a command to the Arduino over serial.) The Arduino output pin is connected to a relay switch (SainSmart) which gives a 12 V pulse to the solenoid. ALSO the object sends a signal to the LED driver to change from red to green or vica versa. (Note by “turnout object” I mean the JMRI virtual representation of the physical turnout).

There are two MEGAs: One just to send the output digital voltage pulses to the relay. The second MEGA is used to sense the switches and drive the LEDs. The second MEGA can be mounted on the panel. The first can by inside the control box.

The LEDs are driven by a 16 channel LED driver over IC2.

## System schematic


![alt text](https://gitlab.com/pjbca/dccplusplusphysicalpanel/-/raw/master/PhysicalControlPanel.svg)



# OS install (get Raspberry Pi setup first):
* Install latest version of Rasbpian
```
sudo raspi-config
```
* Get on internet.
```
sudo emacs /etc/dhcpcd.conf
hostnamectl set-hostname 'trainpi'
```
* Update	
```
sudo apt update
```
* Install emacs.
```
sudo apt-get install emacs
```
* Fix screen resolution (edit this file):
```
sudo emacs /boot/config.txt
```
* Install realvnc.
```
sudo apt install realvnc-vnc-server realvnc-vnc-viewer
```
* Install conky (use your own conky.conf file).
```
   sudo apt-get install conky -y
   cp conky.conf /etc/conky/conky.conf
   sudo cp conky.conf /etc/conky/conky.conf
   sudo emacs /usr/bin/conky.sh
   sudo emacs  /etc/xdg/autostart/conky.desktop
```
* Install speedtest
```
sudo apt-get install speedtest-cli
```
* Install bashtop
```
git clone https://github.com/aristocratos/bashtop.git
cd bashtop
sudo make install
```
* Check JAVA version
```
   java -version
```
* Get rid of pwd warning:
```
   cd /etc/profile.d/
   sudo rm sshpwd.sh
```
* Install JMRI

Download JMRI from http://jmri.org/download/index.shtml and save the file.

Double click the downloaded tarball, that will open the archive manager program. Extract the JMRI tarball to your home directory, or some other directory. The extract program will create a JMRI folder in this location.

JMRI can now be run by double clicking on the DecoderPro3 icon in the JMRI folder. You have the option of simply running the program or running in Terminal, I would suggest running it in Terminal as it will show any error messages in the terminal window.

JMRI will ask for preferences to be set up, select your Manufacturer of your DCC system (eg. Digitrax, NCE, etc.), then select how you are connecting to the DCC system (LocoBuffer, LocoNet PR3, etc.)

Then select the serial port from the drop down, it should be something like "/dev/ttyACM0"

Now select your command station type, (DCS50, PR3, etc.) leave the Connection Prefix and Connection Name they way they are.
It will ask you to restart JMRI. After the restart of JMRI, if all goes well, you should be running JMRI.

* Create desktop icon for JMRI (edit a few files).
```
   emacs desktop.txt
   cp desktop.txt ~/Desktop/JMRIlaunch.desktop
   rm desktop.txt
   cp JMRI.png ~/Documents/JMRI/
```
* File JMRIlaunch.desktop should be like:
```
[Desktop Entry]
Type=Application
Name=JMRI
GenericName=JMRI
Comment=JMRI
Exec="/home/pi/Documents/JMRI/PanelPro"
Icon=/home/pi/Documents/JMRI/JMRI.png
Terminal=false
```
* Install Arduino IDE:

Download IDE from website, 
extract into /home/peter/documents/Arduino
run install.sh
Puts icon on desktop.
* Setup so history is recorded from all terminals

Bash history http://northernmost.org/blog/flush-bash_history-after-each-command/
basically put this export PROMPT_COMMAND='history -a'
 into /etc/bash.bashrc
* Install from software manager Meld (compares files, useful for code dev)

# Board USB interface configure:
I have three Arduino MEGAs. Each time I plug it in or power up, it is random which device id the system assigns to it, e.g. /dev/ttyACM0 /dev/ttyACM1 /dev/ttyACM2. So we have to create a symbolic link to each one based on its serial #. The symbolic links for some reason have to be of the format /dev/tty100 /dev/tty101 /devtty102 etc.

Figure out board ID after plugin in Adriuno Mega to USB:
```
   lsusb >> l1.txt
   lsusb >> l2.txt
   diff l1.txt l2.txt
   ls /dev/tty*
```
* Change permissions /dev/ttyACM0 (Mega for DCC)
```
sudo chmod a+rw /dev/ttyACM0
```
* Plug in second MEGA (for turnouts and panel):

Bus 001 Device 004: ID 2341:0042 Arduino SA Mega 2560 R3 (CDC ACM)
/dev/ttyACM1
* Set permissions for second MEGA:
```
sudo chmod a+rw /dev/ttyACM1
```
* Make sure ACM1 and ACM0 are not mixed up. (in progress)

See: http://hintshop.ludvig.co.nz/show/persistent-names-usb-serial-devices/
Put the rules file here: /etc/udev/rules.d/99-usb-serial.rules
My rules file for Mega for panels:

SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="75830333439351A0B001", SYMLINK+="panelmega"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="75830333439351A0B001", SYMLINK+="tty100"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="55838343034351D0A2C1", SYMLINK+="dccmega"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="55838343034351D0A2C1", SYMLINK+="tty101"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="758303339373516081D1", SYMLINK+="tty102"

SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="75830333439351A0B001", SYMLINK+="panelmega"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="75830333439351A0B001", SYMLINK+="tty100"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="55838343034351D0A2C1", SYMLINK+="dccmega"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="55838343034351D0A2C1", SYMLINK+="tty101"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0042", ATTRS{serial}=="758303339373516081D1", SYMLINK+="tty102"
SUBSYSTEM=="tty", ATTRS{idVendor}=="04d8", ATTRS{idProduct}=="ff7e", ATTRS{serial}=="5200C1EB", SYMLINK+="tty103"



tty100 is mega1 (Will be for relays only)

tty101 is DCC Mega

tty102 is Mega2 (Will be mounted to panel for switch inputs and LED outputs)

tty103 is DSC52 Digitrax Zephyr (Throttle/control station connected to tracks)

* Check it worked:
```
pi@trainpi:~ $ ls -l /dev/tty101
```

Note you have to put “/dev/tty101” in the python script.


# Software installation on MEGAs:
Install on each MEGA the sketch listed as:
 PhysicalPanelMEGA1.ino
 PhysicalPanelMEGA2.ino

# JMRI configuration:

* Double click “decoder pro”

* Need to create turnouts, sensors. There is a script to do that by Geoff or you can do it manually as per his blog entry. Turnout is ATxx where xx=2 to 99. Sensor is ARxx where xx=2 to 99.

* Download the jython script file  PhysicalPanelCode2MEGAs.py into a directory of your choice.

* Create a button under preferences-> startup to run a script. The script should be the file you downloaded. Click save. JMRI will ask to restart, hit yes.

* Start JMRI. Run the script by pressing on the button. 

You are up and running.
You can check the status by panel-> output monitor.
Also you can open a turnout and sensor table to monitor them as you throw the switches.


# How the code works:
## Arduino MEGA1:
* In setup()
Opens serial port
Defines pins 2-69 digital outputs, sets them HIGH
* In loop()
Listens on serial for ATxxy, where xx is 2-69 and y is 0 or 1
Changes HI/LO for corresponding pin

## Arduino MEGA2:
* In setup()
   * Creates 4 LED driver objects PWM1 PWM2 PWM3 PWM4
   * Waits for !!! over serial port before finishing setup (Jython must do this)
   * Opens serial port
   * Defines pins 2-69 digital inputs (pullup hi), reads them into array
   * Send their state over serial to JMRI as Ax
* In loop()
   * Scan sensor input pins
   * Report if changed to JMRI over serial (AX, where X is ...)
   * Listens on serial for ATxxy, where xx is 2-69 and y is 0 or 1
   * Write output to LED driver
   * Manually mapped turnout # to the LED number on driver to make hi or low.
## Jython:
* Defines, creates n “TurnoutChangeListener” objects (one for each turnout).
   * When turnout changes sends ATxx pulse to MEGA1 to open relay and throw physical switch
xx is either thrownrelayoutputpinnumber or closedrelayoutputpinnumber
sends ATxx to MEGA2 (xx turnout #) to allow it to change panel LEDs
* Defines, creates n “SensorToThrowTurnoutListener” and  “SensorToCloseTurnoutListener” objects (one for each turnout).
   * On sensor change (sensornumber), change corresponding turnout  object in JMRI(turnoutnumber) to CLOSED or THROWN
* Creates a single instance of “SerialSensorMux”, an automaton subclass
* Sends !!! at start/init
* Reads serial, if "A" in byte, parses
* Sets sensor object in JMRI based on read serial message
* Initiates instance of “SerialSensorMux”

# Architecture (software):
* JMRI OBJECTS (per turnout)
   * TURNOUT
   * SENSORTOTHROW
   * SENSORTOCLOSE
* JMRI OBJECTS FOR CONTROL
   * SERIAL MUX (ONE ONLY) Listens to serial for AXXXon/off and changes SENSOR object in JMRI
* Adruino MEGA1 pins (per turnout)
   * OUTPUTTOTHROWRELAY
   * OUTPUTTOCLOSERELAY
* Adruino MEGA2 pins (per turnout)
   * INPUTTOTHROW
   * INPUTTOCLOSE


## SEQUENCE OF EVENTS TO THROW SWITCH (A=Arduino, J=JMRI)
* (A) User throws manual momentary switch, brings a pin on MEGA2 mometarily LOW (pins are pulled up high)
* (A) MEGA2 sends AXXhi/lo over serial to JMRI XX= pin number
* (J) SerialSensorMux object reads serial, changes corresponding SENSOR
* (J) SensorToThrowTurnoutListener or SensorToCloseTurnoutListener senses SENSOR change, causes TURNOUT change
* (J) TurnoutChangeListener ATXX0 then 0.1s later ATXX1 to MEGA1. XX=thrownrelayoutputpinnumber or closedrelayoutputpinnumber (MEGA1)
AND sends ATXX0,1 to MEGA2 for it to change LED panel lights (xx turnout #).
* (A) MEGA1 sends 0.1s long LOW pulse on pin thrownrelayoutputpinnumber or closedrelayoutputpinnumber to throw/close physical turnout through relay.
* (A) MEGA2 changes LED panel lights through LED driver (hard coded turnout # to LED driver)


## DCC++

Put the file (unzipped) DDCBurke.zip in your Arduino Libraries directory, this is the version of DCC++ I am using.
It is a mod of the original DCC++ with the DCC decoder file changed for better handling of Digitrax that I read on a blog somewhere.

## Running JMRI automatically

I want the whole system to startup when I power everything on, without having to log into the computer. To do this, we create a service using systemd on Linux.

* Create a file called trains.service
```
sudo emacs /etc/systemd/system/trains.service
```
* The contents of the file should like like:
```
[Service]
User=pi
ExecStart = /home/pi/Documents/JMRI/JmriFaceless --profile=My_JMRI_Railroad.3f4b
c289

[Install]
WantedBy=multi-user.target
```
   * Note the “HOME” variable needs to be manually set in JmriFaceless as /home/pi on line 222
   ```
   sudo emacs /home/pi/Documents/JMRI/JmriFaceless
   ```
   * You will have to find your profile name in profile.xml.
   * Substitute the appropriate values from .jmri/profiles.xml.
   * You need to pick user=pi or your service will run as root and end up in the wrong directory and won’t find the JMRI profiles it needs.

### To start the service:
```
sudo systemctl start trains
```
### To stop the service:
```
sudo systemctl stop trains
```
### To check the service status:
```
sudo systemctl status trains
```
### To see the log file:
```
tail /var/log/syslog
```
### To make the service start automatically on boot:
```
sudo systemctl enable trains
```
### To turn off the service starting automatically on boot:
```
sudo systemctl disable trains
```


# A note on Sainsmart relay board and boot order:

On power off, the pins of Arduino are ground.
On Sainsmart, ground at the input causes 4 mA to flow, so we don't want this for all pins all the time.
Therefore, I choose the branch of the relay (it is 3 pins, the center has 12 V, and it is shorted to one or the other) so that when the input is ground, 12 V is delivered to switch.
Since the voltage is only ever delivered to the switch for 0.1 seconds at a time, this means most of the time, the inputs will be high on ths Sainsmart.
The digital high input draws no current. Therefore we don't have to worry about 4 mA per pin times 70 pins. In steady state, all Aruino pins are high, the input of the Sainsmart digital draws no current from the Arduino, and the relays do not deliver 12 V to the turnout solonoids.

The problem is at power on, before the Arduino is booted up, the Sainsmart gets its 12 V and sees a low at the (still not booted) Arduino digital output pins.
This causes ALL the relays to deliver continuous 12 V to ALL the solonoids, which will either burn all the solonoids are (in my case) trip the short circuit on the 12 V supply.

So the solution is to use Sainsmart board D (which was until now not needed) to be the 12 V supplier for all the other 3 Sainsmarts.

It will have pin 16 (Sainsmart) connected to Arduino Mega pin 46. On boot, Mega is low, and Sainsmart D will NOT deliver 12 V to the other 3 Sainsmarts (A-C).
After boot, when the user (sketch) decides it is safe, it can put pin 46 high. This will cause Sainsmart D to deliver 12V to all the other 3 Sainsmarts. This should only be done AFTER all the other Mega pins are set high so there is no power to the solonoids when the Sainsmart A-C are powered up.

# To run train over website:

* Need to create a "panel" for layout.
* Need to run JMRI as UI application. (Can't use headless mode, it seems....)
```
sudo systemctl disable trains
```
```
sudo reboot now
```
* Log back in through VNC, start JMRI manually
* Now you can log into JMRI server by using a browswer and going to:
   * piipaddress:12080
   * In my case, the hostname has been set as trainpi, so goto http://trainpi.local:12080
* On the web browswer:
   * You can pull up the panel to throw the turnouts. Just click on the turnout with the mouse and it will change it and show on the figure.
   * You can run throttles using the "web throttle".
* If you want to run it over internet, you need to map the IP address of your router in the internet to the local IP address, and enable the firewall to access the port 12080. (Do this at your own risk!!!)

## Web server screenshots

![alt text](https://gitlab.com/pjbca/dccplusplusphysicalpanel/-/raw/master/figures/Screenshot_from_2021-04-10_22-46-22.png)

![alt text](https://gitlab.com/pjbca/dccplusplusphysicalpanel/-/raw/master/figures/Screenshot_from_2021-04-10_22-46-58.png)

![alt text](https://gitlab.com/pjbca/dccplusplusphysicalpanel/-/raw/master/figures/Screenshot_from_2021-04-10_22-47-17.png)

![alt text](https://gitlab.com/pjbca/dccplusplusphysicalpanel/-/raw/master/figures/Screenshot_from_2021-04-10_22-47-31.png)




