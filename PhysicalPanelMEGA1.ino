// PhysicalPanel
// Designed to go along with jython JMRI script xxx and an Arduino.
// For monitoring switches on a panel, lighting LED of the panel , and throwing turnouts.
// Peter Burke 6/11/2020
// Adapted from Geoff Bunza 2018
// url https://model-railroad-hobbyist.com/node/34417
// url https://model-railroad-hobbyist.com/node/34392



//*******************CONSTANTS**********************************************


#define Data_Pin_Max    70  // Max sensor pin NUMBER (plus one) Mega=70,UNO,Pro Mini,Nano=20
#define Data_Pin_Start   2  // Starting Sensor Pin number (usually 2 as 0/1 are TX/RX
#define Data_Pin_Offset  0  // This Offset will be ADDED to the value of each Turnout number to determine the Data pin
                            // number used by the Arduino, so pin AT12 will set pin 12+Data_Offset)
                            // This would allow one Arduino Turnout Data channel to use AT2-69 set pins 2-69 and another to 
                            // use AT70-137 to set its pins 2-69 for example; this offset can also be negative
#define Data_Invert      1  // Set Data_Active_Low to 1 to invert incomin Turnout data Closed=1 Thrown=0
                            // Set Data_Active_Low to 0 to leave incoming data untouched Closed=0 Thrown=1

#define open_delay 15        // longer delay to get past script initialization
#define delta_delay 4        // Short delay to allow the script to get all the characters

#define port_delay 600      // Serial Port delay (microsec) for error compensation - increase this if you get funny data at high speeds


//**********************VARIABLES****************************************8

int i;


int j;
char  firstByte ;           // temp to process the possible state change
char  secondByte ;
char  incomingByte = 0;     // working temp for character processing
int   turnout = 99;


//*******************SETUP**********************************************

void setup() {


//*******************Turnouts setup*****************************************
//  Initialize pins Data_Pin_Start to Data_Pin_Max as OUTPUT
//  Set them all HIGH.

    for (j=Data_Pin_Start; j< Data_Pin_Max; j++) {  // Set up all the output pins
      pinMode (j, OUTPUT);
      digitalWrite (j, HIGH);
  } 

    
    Serial.begin(19200);              // Open serial connection.


}

//*******************LOOP**********************************************

void loop() {
//*******************Turnouts code***************************************** 

 
  // get the first character
      firstByte = Serial.read();
      delayMicroseconds(port_delay);
      while ((firstByte != 'A') && (Serial.available())) { // bad code, gets hung here if A never appears
        firstByte = Serial.read();
        delayMicroseconds(port_delay);
      }      
      if (Serial.available()) {secondByte = Serial.read();}       // get the second character
      if ((firstByte=='A') && (secondByte == 'T'))  {
        j=0;
        turnout = Serial.parseInt();
        turnout = turnout + Data_Pin_Offset ;
        if (Serial.available()) {incomingByte=Serial.read();}
        delayMicroseconds(port_delay);
        if (Serial.available()) {incomingByte=Serial.read();}
        if (incomingByte =='1') digitalWrite (turnout, 1 ^ Data_Invert);
        if (incomingByte =='0') digitalWrite (turnout, 0 ^ Data_Invert);
      }
  
 
}
